module.exports = {
  okapi: { 'url':'http://localhost:9130', 'tenant':'diku' },
  config: {
    // autoLogin: { username: 'diku_admin', password: 'admin' }
    // logCategories: 'core,redux,connect,connect-fetch,substitute,path,mpath,mquery,action,event,perm,interface,xhr'
    // logPrefix: 'stripes'
    // logTimestamp: false
    // showPerms: false
    showHomeLink: true,
    // listInvisiblePerms: false
    // disableAuth: false
    welcomeMessage: 'mydevplatform.front.welcome',
    platformName: 'MyDevPlatform',
    platformDescription: 'my dev platform',
    hasAllPerms: true, // NOTE THIS!!!!
    showDevInfo: true,
    languages: ['en'],
  },
  modules: {
    '@folio/users': {},
    '@folio/developer': {},
    "@folio/tenant-settings": {},
    "@folio/inventory": {},
  },
  branding: {
    logo: {
      src: './tenant-assets/k-int.png',
      alt: 'K-Int Sandbox',
    },
    favicon: {
      src: './tenant-assets/k-int-favicon.png',
    },
  },
};
