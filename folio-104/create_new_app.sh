#!/bin/bash

cd localbuild

# Project assets
mkdir ui-ems
cd ui-ems

pwd

echo copying package.json
cp -R ../../resources/package.json .
cp -R ../../resources/src .
cp -R ../../resources/translations .


echo Made app, add    "@knowledge-integration/ems": "^1.0.0",      to platform package.json and   @knowledge-integration/ems    to stripes.config.json
echo then yarn install
echo then yarn run stripes serve stripes.config.js

