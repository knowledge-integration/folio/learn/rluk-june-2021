#!/bin/bash

# This script is a helper when trying to track down rogue dependencies - it will give you a build you can
# use to track down why dependencies are or are not getting hoisted


mkdir localbuild
cd localbuild
# If you want to develop a module add it's git clone command here to check the module out
# into your development workspace - your checked out module will be used in preference to the
# provided dependency
#
# Some examples:
#
# git clone https://github.com/folio-org/ui-dashboard.git
# git clone https://github.com/k-int/ui-ciim.git
# git clone https://github.com/folio-org/ui-agreements.git
# git clone https://github.com/folio-org/ui-licenses.git
# git clone https://github.com/folio-org/ui-local-kb-admin.git
# git clone git@github.com:folio-org/ui-remote-sync.git
# git clone git@github.com:folio-org/ui-oa.git
git clone https://github.com/folio-org/ui-users


# Set up the workspace package.json
echo "{
  \"private\": true,
  \"workspaces\": [
      \"*\"
  ],
  \"devDependencies\": {
    \"@folio/stripes-cli\": \"^1.2.0\"
  },
  \"dependencies\": {
  },
  \"resolutions\": {
  }
}
" > package.json

# Project assets
mkdir mydevfolio
cd mydevfolio
cp ../../package.json .
cp ../../stripes.config.js .
cp -R ../../tenant-assets .

# Build
yarn install

# This is here for reference - helpful when building prod'n bundles
# yarn run stripes mod descriptor --full --output /tmp

# Remind dev how to run the bundle above
echo run in development mode with
echo   cd localbuild/mydevfolio
echo   yarn run stripes serve stripes.config.js
