import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useOkapiKy } from '@folio/stripes/core';
import { useQuery, useMutation } from 'react-query';
import { useStripes } from '@folio/stripes/core';


const propTypes = {
};

const EMSView = ({}) => {

  const ky = useOkapiKy()

  const { tenant, token, url } = useStripes().okapi;

  // const { data: { 0: summary } = [], isLoading: summaryLoading, refetch: refetchSummary } = useQuery(
  const { data, isLoading, refetch } = useQuery(
    ['ui-ems', 'summary'],
    async () => {
      // Actually wait for the data to come back.
      // const query_result = await ky.post('ems/graphql', {json: { "query":"query Query { enquiries { subject body status } }" }, mode:'no-cors' }).json();

      console.log("Fetching....");
      // https://github.com/folio-org/stripes-core/blob/master/doc/okapiKy.md
      const query_result = await ky('ems/graphql', {method:'POST', json: { "query":"query Query { enquiries { subject body status } }" } } );
      const json_qr = query_result.json();
      console.log("Got graphgql response %o",json_qr);
      return json_qr;
    }
  );


  let display_data = "Fetching.....";

  if ( data != null ) {
    display_data = JSON.stringify(data);
  }


  return (
    <div>
      EMS
      Content: <pre>{display_data}</pre>
    </div>
  );
}

EMSView.propTypes = propTypes;


export default EMSView
