import React, { lazy, Suspense } from 'react';
import { Switch } from 'react-router-dom';
import { Route } from '@folio/stripes/core';
import PropTypes from 'prop-types';
const EMSView = lazy(() => import('./EMSView'));


class App extends React.Component {
  static propTypes = {
    actAs: PropTypes.string.isRequired,
    match: PropTypes.object.isRequired,
    stripes: PropTypes.object.isRequired,
  }


  render() {
    const { actAs, match: { path }, stripes } = this.props;

    return (
      <Suspense fallback={null}>
        <EMSView />
      </Suspense>
    );
  }
}

export default App;
