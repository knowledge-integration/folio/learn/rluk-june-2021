const express = require('express');
const { ApolloServer, gql } = require('apollo-server-express');
const bodyParser = require('body-parser')

const typeDefs = gql`
  # Comments in GraphQL strings (such as this one) start with the hash (#) symbol.

  # This "Enquiry" type
  type Enquiry {
    subject: String
    body: String
    status: String
    enquiryType: String
    patronIdAuthority: String
    patronId: String
  }

  # The "Query" type is special: it lists all of the available queries that
  # clients can execute, along with the return type for each. 
  type Query {
    enquiries: [Enquiry]
  }
`;

const enquiries = [
  {
    subject: 'APC Enquiry - An approach to probabalistic reasoning using Quantum Gateways - X-Foundation Funded',
    body: 'Hey Library, super whizzy faculty have funding from X-Foundation (Grant qa337) for our quantum computing programme, but the grant comes with a condition for Open Access publishing. We need a venue that supports X,Y and Z can you help us please?',
    status: 'OPEN',
    enquiryType:'ResearchSupport',
    patronIdAuthority:'FOLIO:PATRON',
    patronId:'1234'
  },
  {
    subject: 'Missing volume 4 of Journal of Cybernetics',
    body: 'I am looking for vol 4 of Journal of Cybernetics, but it is not where it should be on the shelf. Can you help me locate it please?',
    status: 'OPEN',
    enquiryType:'Patron',
    patronIdAuthority:'FOLIO:PATRON',
    patronId:'1238'
  },
];

const resolvers = {
  Query: {
    enquiries: () => enquiries,
  },
};

async function startApolloServer() {
  const app = express();
  const server = new ApolloServer({
    typeDefs,
    resolvers
  });
  await server.start();

  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: true }))

  // Docs: https://expressjs.com/en/guide/using-middleware.html
  server.applyMiddleware({ app,
                           path:'/ems/graphql',
                           cors: false });

  
  app.use('/_/tenant', function (req, res, next) {
    console.log('Path:', req.path)
    switch ( req.path ) {
      case '/':
        switch ( req.method ) {
          case 'POST':
            // http://expressjs.com/en/5x/api.html
            console.log("Tenant create request", req.params, req.body);
            res.status(201);  // return CREATED
            break;

          case 'DELETE':
            console.log("Tenant delete request");
            res.status(204);  // return NO CONTENT
            break;

          default:
            res.status(500);
            break;
        }
        break;
      case '/disable':
        console.log("Tenant disable request");
        res.status(204);  // return NO CONTENT
        break;
   
      default:
        res.status(500);
 
    }

    // res.send('Hello!');
    res.end();
  })

  await new Promise(resolve => app.listen({ port: 4000 }, resolve));
  console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`);
  return { server, app };
}

startApolloServer();
