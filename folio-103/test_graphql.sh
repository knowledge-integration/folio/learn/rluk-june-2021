#!/bin/bash

TENANT="diku"
OKAPI="http://localhost:9130"
AUTH_TOKEN=`./okapi-login`

RESPONSE=`curl --header "X-Okapi-Tenant: ${TENANT}" -H "X-Okapi-Token: ${AUTH_TOKEN}" -H "Content-Type: application/json" -X POST ${OKAPI}/ems/graphql -d '
{
  "query":"query Query { enquiries { subject body status } }"
}
'`

echo $RESPONSE
