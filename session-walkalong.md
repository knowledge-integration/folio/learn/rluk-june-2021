

# FOLIO 101 - Running folio locally

Prereqs : vagrant, virtualbox, Slack

The [Dev] Community:

The FOLIO Slack Channel: #support, #devops, #development, @ianibbo

The Software:

Each release is bundled up into a number of handy Vagrant images - see https://app.vagrantup.com/folio

    cd folio-101
    vi Vagrantfile  -- Explaining the parts of the file wrt memory, vcpus and ports, note folio-snapshot
    vagrant up

    Signpost to managing vagrant boxes
      - vagrant destroy
      - vagrant box list
      - vagrant box update
      - vagrant box prune

Access the system

    http://localhost:3000
    login diku_admin/admin

    vagrant destroy


# FOLIO 102 - Backend and Frontend for development

Prereqs: vagrant, virtualbox, bash, curl, nvm, default node (v14.15.4)

A word about running prod systems, vagrant images, permissions, the tenant API endpoint ( slash underscore ), users and passwords

## Establish the backend

    cd folio-102
    vi Vagrantfile  -- New box, lower requirements, 3000 no longer shared
    vagrant up

    vi ./okapi-cmd

    ./okapi-cmd /users

## Establish a frontend

    cd frontend
    ./build-locally.sh
    cd localbuild/mydevfolio
    yarn run stripes serve stripes.config.js


Explain package.json, stripes.config.js relation and workspace


# FOLIO 103 - Backend module development "mod-ems"

- What will our new module do
- Module Descriptors
- Module activation
- Implementation Detail
    - This is not a normal FOLIO module
- Module packaging

## Lets make our app

    mkdir mod-ems
    cd mod-ems
    npm init -y
    npm install apollo-server-express graphql
    cp ../resources/first_index.js ./index.js
    node index.js

### Test graphql
visit https://studio.apollographql.com/sandbox and Add http://localhost:4000/ems/graphql as target
or http://localhost:4000/ems/graphql

    query enquiries {
      enquiries {
        subject
      }
    }


Exercise the tenant API
    curl -X POST localhost:4000/_/tenant
    curl -X DELETE localhost:4000/_/tenant
    curl -X POST localhost:4000/_/tenant/disable

## Module Activation

    ./register-and-enable.sh

Watch the console 

## Test endpoint via okapi

    ./test_graphql.sh


# Episode 4: A new client

    cd folio-104
    ./build_locally.sh
    ./create_new_app.sh
    cd localbuild/mydevfolio
    yarn install

edit package.json / stripes.config.json

    yarn run stripes serve stripes.config.js

    


# Summary

We have (1) run a largely immutable local test system
(2) created a local install of a backend and frontend build
(3) Extended the APU offered at the API gateway with a new resource /ems
(4) Created a new frontend app to access that resource
